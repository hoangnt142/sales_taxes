require 'spec_helper'
require 'csv'

describe 'Receipt' do

  it 'Receipt 1 is 29.83' do
    sales_taxes = Receipt.new("./input1.csv").calculate
    sales_taxes.should eq(29.83) 
  end

  it 'Receipt 1 is 65.15' do
    sales_taxes = Receipt.new("./input2.csv").calculate
    sales_taxes.should eq(65.15) 
  end

  it 'Receipt 1 is 74.68' do
    sales_taxes = Receipt.new("./input3.csv").calculate
    sales_taxes.should eq(74.68) 
  end

end 