require 'csv'

class Receipt

  def initialize(input_csv)
    @input_csv = input_csv
    @products = []

    CSV.foreach(input_csv) do |row|
      @products << {
        quantity: row[0],
        name: row[1],
        price: row[2]
      }
    end
  end

  def calculate
    receipt_products = []

    receipt_not_included_tax = 0
    receipt_included_tax = 0

    products.each do |product|

      product_price_with_tax = include_tax_price(product[:name], product[:price])

      receipt_products << {
        quantity: product[:quantity],
        name: product[:name],
        price: product_price_with_tax
      }

      receipt_not_included_tax += product[:price].to_f
      receipt_included_tax += product_price_with_tax
    end

    output_file_name = "output#{input_csv[/\d+/].to_i}.csv"

    sale_tax = (receipt_included_tax - receipt_not_included_tax).round(2)

    CSV.open(output_file_name, "w") do |csv|
      receipt_products.each do |product|
        csv << [product[:quantity], product[:name], product[:price].round(2)]
      end
      csv << ['Sales Taxes', sale_tax]
      csv << ['Total', receipt_included_tax.round(2)]
    end

    receipt_included_tax.round(2)
  end

  def include_tax_price(product_name, price)
    tax_rate = 0
    total_price = price.to_f

    if !exempt?(product_name)
      tax_rate += 0.10
    end

    if imported?(product_name)
      tax_rate += 0.05
    end

    tax = (total_price * tax_rate)
    total_price + round(tax)
  end

  def round(tax)
    round_factor = 0.05
    ((tax / round_factor).ceil * round_factor).round(2)
  end

  def exempt?(product_name)
    ['book', 'chocolate', 'pills'].each do |good|
      return true if product_name.include?(good)
    end
    return false
  end

  def imported?(product_name)
    !product_name.index('import').nil?
  end

  attr_reader :products, :input_csv

end